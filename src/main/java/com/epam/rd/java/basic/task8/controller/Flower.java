package com.epam.rd.java.basic.task8.controller;

public class Flower {
    private String name;
    private Soil soil;
    private String origin;

    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private Multiplying multiplying;

    enum Soil{
        POD("подзолистая"),
        GRU("грунтовая"),
        DER_POD("дерново-подзолистая");

        String name;
        Soil(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        static Soil soilOf(String value){
            for (Soil soil : values()) {
                if (soil.getName().equalsIgnoreCase(value))
                    return soil;
            }
            throw new IllegalArgumentException();
        }

        @Override
        public String toString() {
            return this.getName();
        }
    }
    enum Multiplying{
        LEAVES("листья"),
        CHER("черенки"),
        SEEDS("семена");

        String name;
        Multiplying(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        static Multiplying multiplyingOf(String value){
            for (Multiplying mul : values()) {
                if (mul.getName().equalsIgnoreCase(value))
                    return mul;
            }
            throw new IllegalArgumentException();
        }

        @Override
        public String toString() {
            return this.getName();
        }
    }

    public String getName() {
        return name;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Soil getSoil() {
        return soil;
    }

    public void setSoil(Soil soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public Multiplying getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(Multiplying multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name= " + name +
                ", soil= " + soil +
                ", origin= " + origin +
                ", visualParameters= " + visualParameters +
                ", growingTips= " + growingTips +
                ", multiplying= " + multiplying +
                '}';
    }
}

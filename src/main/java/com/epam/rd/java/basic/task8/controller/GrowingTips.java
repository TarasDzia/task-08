package com.epam.rd.java.basic.task8.controller;

public class GrowingTips {
    private int tempreture;
    private LightRequiring lighting;
    private int watering;

    enum LightRequiring{
        yes,no;

        @Override
        public String toString() {
            return this.name();
        }
    }

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public LightRequiring getLighting() {
        return lighting;
    }

    public void setLighting(LightRequiring lighting) {
        this.lighting = lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}

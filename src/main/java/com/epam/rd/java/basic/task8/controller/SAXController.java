package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private String xmlFileName;

	private List<Flower> flowers;
	private StringBuilder elementValue;
	private VisualParameters vparam;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE


	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (elementValue == null) {
			elementValue = new StringBuilder();
		} else {
			elementValue.append(ch, start, length);
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		switch (qName) {
			case Constants.FLOWERS:
				flowers = new ArrayList<>();
				break;
			case Constants.FLOWER:
				flowers.add(new Flower());
				break;
			case Constants.VISUAL_PARAMETERS:
				latestFlower().setVisualParameters(new VisualParameters());
				break;
			case Constants.GROWING_TIPS:
				latestFlower().setGrowingTips(new GrowingTips());
				break;
			case Constants.LIGHTING:
				final String lightRequiring = attributes.getValue("lightRequiring");
				latestFlower().getGrowingTips().setLighting(GrowingTips.LightRequiring.valueOf(lightRequiring));
				break;
			default:
				elementValue = new StringBuilder();
				break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
			case Constants.NAME:
				latestFlower().setName(elementValue.toString());
				break;
			case Constants.SOIL:
				latestFlower().setSoil(Flower.Soil.soilOf(elementValue.toString()));
				break;
			case Constants.ORIGIN:
				latestFlower().setOrigin(elementValue.toString());
				break;
			case Constants.STEM_COLOUR:
				latestFlower().getVisualParameters().setStemColour(elementValue.toString());
				break;
			case Constants.LEAF_COLOUR:
				latestFlower().getVisualParameters().setLeafColour(elementValue.toString());
				break;
			case Constants.AVE_LEN_FLOWER:
			latestFlower().getVisualParameters().setAveLenFlower(Integer.parseInt(elementValue.toString()));
				break;
			case Constants.TEMPRETURE:
			latestFlower().getGrowingTips().setTempreture(Integer.parseInt(elementValue.toString()));
				break;
			case Constants.WATERING:
			latestFlower().getGrowingTips().setWatering(Integer.parseInt(elementValue.toString()));
				break;
			case Constants.MULTIPLYING:
			latestFlower().setMultiplying(Flower.Multiplying.multiplyingOf(elementValue.toString()));
				break;
		}
	}

	private Flower latestFlower() {
		return flowers.get(flowers.size()-1);
	}

	public List<Flower> getResult() {
		return flowers;
	}

}
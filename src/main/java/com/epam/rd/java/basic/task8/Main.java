package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.loadDocument();
		List<Flower> flowerList = domController.parse();

		// sort (case 1)
		// PLACE YOUR CODE HERE
		final List<Flower> sortedByName = flowerList.stream().sorted(Comparator.comparing(Flower::getName))
				.collect(Collectors.toList());

		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.buildDocument();
		domController.toXml(sortedByName, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		saxParser.parse(xmlFileName, saxController);
		flowerList = saxController.getResult();

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		final List<Flower> sortedBySoil = flowerList.stream()
				.sorted(Comparator.comparingInt(f -> f.getGrowingTips().getTempreture()))
				.collect(Collectors.toList());
		System.out.println(sortedBySoil);
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		domController.buildDocument();
		domController.toXml(sortedBySoil, outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		// PLACE YOUR CODE HERE
		STAXController staxController = new STAXController(xmlFileName);
		staxController.loadDocument();
		final List<Flower> flowers = staxController.parse();

		// sort  (case 3)
		// PLACE YOUR CODE HERE
		final List<Flower> sortedByMultiplying = flowerList.stream()
				.sorted(Comparator.comparing((f) -> f.getGrowingTips().getWatering()))
				.collect(Collectors.toList());

		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		domController.buildDocument();
		domController.toXml(sortedBySoil, outputXmlFile);
	}

}

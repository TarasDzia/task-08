package com.epam.rd.java.basic.task8.controller;


import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private Document document;
	private static final String measure = "measure";

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void loadDocument() throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		try{
			 document = builder.parse(new File(xmlFileName));
			 document.getDocumentElement().normalize();
		} catch (SAXException | IOException e) {
			System.err.println("File not found");
			e.printStackTrace();
		}
	}

	public void buildDocument() throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		document = builder.newDocument();
	}

	public List<Flower> parse(){
		if(document == null) {
			System.out.println("Document didnt loaded");
			return null;
		}
		List<Flower> result = new ArrayList<>();

		Element root = document.getDocumentElement();
		NodeList nList = document.getElementsByTagName("flower");

		for (int i = 0; i < nList.getLength(); i++) {
			Node node = nList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				//Print each employee's detail
				final Element eElement = (Element) node;

				String name = eElement.getElementsByTagName("name").item(0).getTextContent();
				String soil = eElement.getElementsByTagName("soil").item(0).getTextContent();;
				String origin = eElement.getElementsByTagName("origin").item(0).getTextContent();;

				final Element visualParameters =  (Element) eElement.getElementsByTagName("visualParameters").item(0);
				String stemColour = visualParameters.getElementsByTagName("stemColour")
						.item(0).getTextContent();
				String leafColour = visualParameters.getElementsByTagName("leafColour")
						.item(0).getTextContent();
				String aveLenFlower = visualParameters.getElementsByTagName("aveLenFlower")
						.item(0).getTextContent();

				final Element growingTips = (Element) eElement.getElementsByTagName("growingTips").item(0);
				String tempreture = growingTips.getElementsByTagName("tempreture")
						.item(0).getTextContent();
				String lighting = growingTips.getElementsByTagName("lighting")
						.item(0).getAttributes().item(0).getTextContent();
				String watering = growingTips.getElementsByTagName("watering")
						.item(0).getTextContent();

				String multiplying = eElement.getElementsByTagName("multiplying").item(0)
						.getTextContent();
//-----------------------------------------------------------------------------------------------------------------------
				Flower flower = new Flower();
				flower.setName(name);
				flower.setOrigin(origin);
				flower.setSoil(Flower.Soil.soilOf(soil));

				VisualParameters vParam = new VisualParameters();
				vParam.setStemColour(stemColour);
				vParam.setLeafColour(leafColour);
				vParam.setAveLenFlower(Integer.parseInt(aveLenFlower));

				GrowingTips gTips = new GrowingTips();
				gTips.setTempreture(Integer.parseInt(tempreture));
				gTips.setLighting(GrowingTips.LightRequiring.valueOf(lighting));
				gTips.setWatering(Integer.parseInt(watering));

				flower.setVisualParameters(vParam);
				flower.setGrowingTips(gTips);
				flower.setMultiplying(Flower.Multiplying.multiplyingOf(multiplying));

				result.add(flower);
			}
		}
		return result;
	}

	public void toXml(List<Flower> flowers, String pathName){
		Element rootElement = document.createElement("flowers");

		final Attr xmlns = document.createAttribute("xmlns");
		xmlns.setTextContent("http://www.nure.ua");
		final Attr xsi = document.createAttribute("xmlns:xsi");
		xsi.setTextContent("http://www.w3.org/2001/XMLSchema-instance");
		final Attr scheme = document.createAttribute("xsi:schemaLocation");
		scheme.setTextContent("http://www.nure.ua input.xsd ");

		rootElement.setAttributeNode(xmlns);
		rootElement.setAttributeNode(xsi);
		rootElement.setAttributeNode(scheme);
		document.appendChild(rootElement);

		for (Flower f : flowers) {
			final Element flowerEl = document.createElement("flower");
			rootElement.appendChild(flowerEl);
			final Element nameEl = document.createElement("name");
			final Element soilEl = document.createElement("soil");
			final Element originEl = document.createElement("origin");
			nameEl.setTextContent(f.getName());
			soilEl.setTextContent(f.getSoil().toString());
			originEl.setTextContent(f.getOrigin());

//----------visualParameter Object-----------------------------------------------------------------------------
			final Element visualParametersEl = document.createElement("visualParameters");
			final Element stemColour = document.createElement("stemColour");
			final Element leafColour = document.createElement("leafColour");
			final Element aveLenFlower = document.createElement("aveLenFlower");

			final Attr aveLenFlowerMeasure = document.createAttribute(measure);

			aveLenFlowerMeasure.setTextContent("cm");
			stemColour.setTextContent(f.getVisualParameters().getStemColour());
			leafColour.setTextContent(f.getVisualParameters().getLeafColour());
			aveLenFlower.setTextContent(String.valueOf(f.getVisualParameters().getAveLenFlower()));

			visualParametersEl.appendChild(stemColour);
			visualParametersEl.appendChild(leafColour);
			aveLenFlower.setAttributeNode(aveLenFlowerMeasure);
			visualParametersEl.appendChild(aveLenFlower);

//----------growingTipsEl Object-----------------------------------------------------------------------------
			final Element growingTipsEl = document.createElement("growingTips");
			final Element tempreture = document.createElement("tempreture");
			final Attr tempMeasure = document.createAttribute(measure);
			final Attr waterMeasure = document.createAttribute(measure);
			final Element lighting = document.createElement("lighting");
			final Attr lightRequiring = document.createAttribute("lightRequiring");
			final Element watering = document.createElement("watering");

			tempreture.setTextContent(String.valueOf(f.getGrowingTips().getTempreture()));
			lightRequiring.setTextContent(f.getGrowingTips().getLighting().toString());
			watering.setTextContent(String.valueOf(f.getGrowingTips().getWatering()));


			tempMeasure.setTextContent("celcius");
			tempreture.setAttributeNode(tempMeasure);
			growingTipsEl.appendChild(tempreture);

			lighting.setAttributeNode(lightRequiring);
			growingTipsEl.appendChild(lighting);

			waterMeasure.setTextContent("mlPerWeek");
			watering.setAttributeNode(waterMeasure);
			growingTipsEl.appendChild(watering);
//---------------------------------------------------------------------------------------------------------------

			final Element multiplyingEl = document.createElement("multiplying");
			multiplyingEl.setTextContent(f.getMultiplying().toString());

//------Adding all elements to flowerEl----------------------------------------------------------------------------
			flowerEl.appendChild(nameEl);
			flowerEl.appendChild(soilEl);
			flowerEl.appendChild(originEl);
			flowerEl.appendChild(visualParametersEl);
			flowerEl.appendChild(growingTipsEl);
			flowerEl.appendChild(multiplyingEl);
		}

		try (FileOutputStream output = new FileOutputStream(pathName)) {
			writeXml(document, output);
		} catch (IOException | TransformerException e) {
			e.printStackTrace();
		}
	}

	private static void writeXml(Document doc, OutputStream output)
			throws TransformerException {

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result);
	}

}
